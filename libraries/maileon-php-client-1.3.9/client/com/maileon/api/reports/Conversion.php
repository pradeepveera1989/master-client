<?php

/**
 * This class represents a conversion
 *
 * @author Marcus Beckerle | XQueue GmbH | <a href="mailto:marcus.beckerle@xqueue.com">marcus.beckerle@xqueue.com</a>
 */
class com_maileon_api_reports_Conversion extends com_maileon_api_xml_AbstractXMLWrapper
{
    /**
     * @var string
     */
    public $timestamp;

    /**
     * @var long
     */
    public $contactId;

    /**
     * @var string
     */
    public $contactEmail;

    /**
     * @var double
     */
    public $value;

    /**
     * @var string
     */
    public $mailingSentDate;

    /**
     * @var long
     */
    public $mailingId;

    /**
     * @var string
     */
    public $mailingName;

    /**
     * @var long
     */
    public $siteId;

    /**
     * @var string
     */
    public $siteName;

    /**
     * @var long
     */
    public $linkId;

    /**
     * @var string
     */
    public $linkUrl;

    /**
     * @return \em string
     *  containing a human-readable representation of this conversion
     */
    function toString()
    {

        return "Conversion [timestamp=" . $this->timestamp .
        ", contactId=" . $this->contactId .
        ", contactEmail=" . $this->contactEmail .
        ", value=" . $this->value .
        ", mailingSentDate=" . $this->mailingSentDate .
        ", mailingId=" . $this->mailingId .
        ", mailingName=" . $this->mailingName .
        ", siteId=" . $this->siteId .
        ", siteName=" . $this->siteName .
        ", linkId=" . $this->linkId .
        ", linkUrl=" . $this->linkUrl ."]";
    }

    /**
     * Initializes this conversion from an XML representation.
     *
     * @param SimpleXMLElement $xmlElement
     *  the XML representation to use
     */
    function fromXML($xmlElement)
    {
        if (isset($xmlElement->timestamp)) $this->timestamp = $xmlElement->timestamp;
        if (isset($xmlElement->contact_id)) $this->contactId = $xmlElement->contact_id;
        if (isset($xmlElement->contact_email)) $this->contactEmail = $xmlElement->contact_email;
        if (isset($xmlElement->value)) $this->value = $xmlElement->value;
        if (isset($xmlElement->mailing_sent_date)) $this->mailingSentDate = $xmlElement->mailing_sent_date;
        if (isset($xmlElement->mailing_id)) $this->mailingId = $xmlElement->mailing_id;
        if (isset($xmlElement->mailing_name)) $this->mailingName = $xmlElement->mailing_name;
        if (isset($xmlElement->site_id)) $this->siteId = $xmlElement->site_id;
        if (isset($xmlElement->site_name)) $this->siteName = $xmlElement->site_name;
        if (isset($xmlElement->link_id)) $this->linkId = $xmlElement->link_id;
        if (isset($xmlElement->link_url)) $this->linkUrl = $xmlElement->link_url;
    }

    /**
     * @return \em csv string
     *  containing a csv pepresentation of this conversion
     */
    function toCsvString()
    {
        return $this->timestamp .
        ";" . $this->contactId .
        ";" . $this->contactEmail .
        ";" . $this->value .
        ";" . $this->mailingSentDate .
        ";" . $this->mailingId .
        ";" . $this->mailingName .
        ";" . $this->siteId .
        ";" . $this->siteName .
        ";" . $this->linkId .
        ";" . $this->linkUrl;
    }

    /**
     * For future use, not implemented yet.
     *
     * @return \em SimpleXMLElement
     *  containing the XML serialization of this object
     */
    function toXML()
    {
        // Not implemented yet.
    }
}
