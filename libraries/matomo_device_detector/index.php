<?php


require_once 'device-detector/vendor/autoload.php';

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

// OPTIONAL: Set version truncation to none, so full versions will be returned
// By default only minor versions will be returned (e.g. X.Y)
// for other options see VERSION_TRUNCATION_* constants in DeviceParserAbstract class
DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_NONE);

// Google Bot
//$userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)";
$userAgent = $_SERVER[ 'HTTP_USER_AGENT' ]; // change this to the useragent you want to parse

$dd = new DeviceDetector($userAgent);

// OPTIONAL: Set caching method
// By default static cache is used, which works best within one php process (memory array caching)
// To cache across requests use caching in files or memcache
// $dd->setCache(new Doctrine\Common\Cache\PhpFileCache('./tmp/'));

// OPTIONAL: Set custom yaml parser
// By default Spyc will be used for parsing yaml files. You can also use another yaml parser.
// You may need to implement the Yaml Parser facade if you want to use another parser than Spyc or [Symfony](https://github.com/symfony/yaml)
// $dd->setYamlParser(new DeviceDetector\Yaml\Symfony());

// OPTIONAL: If called, getBot() will only return true if a bot was detected  (speeds up detection a bit)
// $dd->discardBotInformation();

// OPTIONAL: If called, bot detection will completely be skipped (bots will be detected as regular devices then)
// $dd->skipBotDetection();

$dd->parse();
$is_bot = false;
$redirect_url = '';
if ($dd->isBot()) {
    // handle bots,spiders,crawlers,...
    $botInfo = $dd->getBot();
    $is_bot = true;
    $redirect_url = 'https://de.wikipedia.org/wiki/Hanföl';
} else {
    $clientInfo = $dd->getClient(); // holds information about browser, feed reader, media player, ...
    $is_bot = false;
    $redirect_url = 'https://limucan.com';
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Redirect Hanfoel-Secrets Mainpage</title>
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap"
              rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript">
            $( document ).ready(function() {
                let is_bot = "<?php echo $is_bot?>";
                if ( is_bot ) {
                    window.location.href = "https://de.wikipedia.org/wiki/Hanföl";
                } else {
                    let scheme = 'https:';
                    let host_1 = 'li';
                    let host_2 = 'muc';
                    let host_3 = 'an.';
                    let host_4 =  'com';
                    //console.log(scheme + '//' + host_1 + host_2 + host_3 + host_4);
                    window.location.href = scheme + '//' + host_1 + host_2 + host_3 + host_4;
                }
            });
        </script>
        <!--<meta http-equiv="refresh" content="1; URL=<?php /*echo $redirect_url;*/?>" >-->
    </head>

    <body style="border: none; padding: 0px; margin: 0px; background-color: #005a5f;">
        <div style="height: 100vh; width: 100%; background-color: #005a5f; display: table; text-align: center; align-content: center;">
            <div style="display: table-cell; vertical-align: middle;">
                <h1 style="color: #FFF; font-size: 3rem; font-family: 'Roboto Condensed', sans-serif;">SIE WERDEN
                    WEITERGELEITET...</h1>
            </div>
        </div>
    </body>

</html>
