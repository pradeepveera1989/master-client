<?php

/**
 * Wrapper class for Maileon transaction types.
 *
 * @author Viktor Balogh | Wanadis Kft. | <a href="balogh.viktor@maileon.hu">balogh.viktor@maileon.hu</a>
 * @author Marcus St&auml;nder | Trusted Technologies GmbH | <a href="mailto:marcus.staender@trusted-technologies.de">marcus.staender@trusted-technologies.de</a>
 */
class com_maileon_api_transactions_TransactionType extends com_maileon_api_xml_AbstractXMLWrapper
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var array
     */
    public $attributes;

    /**
     *
     * @var integer Archiving duration in days [1..n] after which the transaction events will be deleted, 0 and empty = forever (default)
     */
    public $archivingDuration;

    /**
     * Creates a new transaction type object.
     *
     * @param string $id
     *     the ID of the transaction type
     * @param string $name
     *     the name of the transaction type
     * @param array $attributes
     *     an array of com_maileon_api_transactions_AttributeType attributes associated with the transaction
     * @param integer $archivingDuration
     *     Archiving duration in days [1..n] after which the transaction events will be deleted, 0 and empty = forever (default)
     */
    function __construct(
        $id = null,
        $name = null,
        $attributes = array(),
        $archivingDuration = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->attributes = $attributes;
        $this->archivingDuration = $archivingDuration;
    }

    /**
     * Initializes this transaction type from an XML representation.
     *
     * @param SimpleXMLElement $xmlElement
     *  the serialized XML representation to use
     */
    function fromXML($xmlElement)
    {
        if (isset($xmlElement->id)) $this->id = $xmlElement->id;
        if (isset($xmlElement->name)) $this->name = $xmlElement->name;

        if (isset($xmlElement->attributes)) {
            $this->attributes = array();
            foreach ($xmlElement->attributes->children() as $xmlAttribute) {
                $attribute = array();
                if (isset($xmlAttribute->id)) $attribute['id'] = trim($xmlAttribute->id);
                if (isset($xmlAttribute->name)) $attribute['name'] = trim($xmlAttribute->name);
                if (isset($xmlAttribute->type)) $attribute['type'] = com_maileon_api_transactions_DataType::getDataType($xmlAttribute->type);
                if (isset($xmlAttribute->required)) $attribute['required'] = $xmlAttribute->required;
                if (isset($xmlAttribute->archivingDuration)) $attribute['archivingDuration'] = $xmlAttribute->archivingDuration;
                array_push($this->attributes, $attribute);
            }
        }
    }

    /**
     * @return \em string
     *  a human-readable representation of this object
     */
    function toString()
    {
        // Generate attributes string
        $attributes = "[";
        if (isset($this->attributes)) {
            foreach ($this->attributes as $index => $value) {
                $attributes .= "attribute (id=" . $value['id'] . ", name=" . $value['name'] . ", type=" . $value['type']->getValue() . ", required=" . (($value['required'] == true) ? "true" : "false") . "), ";
            }
            $attributes = rtrim($attributes, ' ');
            $attributes = rtrim($attributes, ',');
        }
        $attributes .= "]";

        return "TransactionType [id=" . $this->id . ", name=" . $this->name . ", archivingDuration=" . $this->archivingDuration . ", attributes=" . $attributes . "]";
    }

    /**
     * @return \em SimpleXMLElement
     *  containing the XML serialization of this object
     */
    function toXML()
    {
        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><transaction_type></transaction_type>");

        // Some fields are mandatory, especially when setting data to the API
        if (isset($this->id)) $xml->addChild("id", $this->id);
        if (isset($this->name)) $xml->addChild("name", $this->name);
        if (isset($this->archivingDuration)) $xml->addChild("archivingDuration", $this->archivingDuration);

        if (isset($this->attributes) && sizeof($this->attributes) > 0) {

            $attributes = $xml->addChild("attributes");
            foreach ($this->attributes as $index => $value) {
                $field = $attributes->addChild("attribute");
                $field->addChild("id", $value->id);
                $field->addChild("name", $value->name);
                $field->addChild("type", $value->type->getValue());
                $field->addChild("required", ($value->required == true) ? "true" : "false");
            }
        }

        return $xml;
    }

    /**
     * @return \em string
     *  containing the XML serialization of this object
     */
    function toXMLString()
    {
        $xml = $this->toXML();
        return $xml->asXML();
    }
}