<?php
// Get the config data from config.ini

use DeviceDetector\DeviceDetector;

require 'config.php';
require 'vendor/autoload.php';

// Get the config data from config.ini       
$conf_obj = new config('config.ini');
$config = $conf_obj->getAllConfig();

$currentUrl = parse_url($_SERVER[ 'REQUEST_URI' ]);

// Keep URL Parameters
$redirect_path = $config[ GeneralSetting ][ KeepURLParameter ] ? $config[ GeneralSetting ][ Redirect ][ RedirectPath ] . $currentUrl[ "query" ] : $config[ GeneralSetting ][ Redirect ][ RedirectPath ];
// Redirect Time
$redirect_time = $config[ GeneralSetting ][ Redirect ][ RedirectTime ];

//$userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)";
$userAgent = $_SERVER[ 'HTTP_USER_AGENT' ];

$dd = new DeviceDetector($userAgent);
$is_bot = false;
$hide_page_for_bots = '';
$replace_links_bott = '';
$dd->parse();
if ($dd->isBot()) {
    $is_bot = true;
    $hide_page_for_bots = $config[ ScambleURL ][ HidePageForBots ];
    $replace_links_bott = $config[ ScambleURL ][ ReplaceLinkForBots ];
}


// Webgains
$webgains = $_GET[ 'trafficsource' ] === "webgains" ? "true" : "false";
$lead_reference = empty($_GET[ 'lead_reference' ]) ? " " : $_GET[ 'lead_reference' ];

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EMS Training zu Hause</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
          type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/style.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
    <base id="mybase" href="">

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>

    <script type="text/javascript">
        $ ( document ).ready ( function () {
            let is_bot = "<?php echo $is_bot?>";
            let replace_links_bott = "<?= $replace_links_bott?>;"
            // replaces all ahref links in page
            if ( replace_links_bott ) {
                document.getElementById ( "mybase" ).href = replace_links_bott;
            }

            if ( is_bot ) {
                // redirect to wikipedia.
                window.setTimeout ( function () {
                    location.href = '<?php echo $hide_page_for_bots?>';
                }, 5000 );
            } else {
                let redirect_page = <?php echo json_encode(str_split($redirect_path, 3))?>;
                let redirect_time = <?php echo $redirect_time; ?>;
                // redirect to shop website.
                window.setTimeout ( function () {
                    location.href = redirect_page.join ( '' );
                }, redirect_time );
            }
        } );
    </script>

    <!-- Google Analytics -->
    <?php include_once 'clients/tracking/google/google.php'; ?>

    <!-- Facebook Pixel -->
    <?php include_once('clients/tracking/facebook/facebook.php'); ?>

</head>


<!-- Outbrain Tracking -->
<?php $config[ TrackingTools ][ EnableOutbrain ] ? include_once 'clients/tracking/outbrain/outbrain_redirect.php' : ' '; ?>

<body>
<!--Adeblo Tracking-->
<?php $config[ TrackingTools ][ EnableAdeblo ] ? include_once 'clients/tracking/adeblo/adeblo.php' : ' ' ?>

<!-- Remarketing Target360 Tracking -->
<?php $config[ TrackingTools ][ EnableTarget360 ] ? include_once 'clients/tracking/target360/target_redirect.php' : ' '; ?>

<!-- FORM -->
<section class=" form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
    <div class="container">
        <div class="row">
            <div class=" tre-content col-sm-12">
                <center>


                    <p>&nbsp;</p>
                    <h1 style="font-size: 40px; font-weight: 600; font-family: 'Titillium Web', sans-serif; color:#fff;">
                        Bitte überprüfe dein E-Mail Postfach.</h1>
                    <p>&nbsp;</p>
                    <p style="font-size: 26px; font-weight: 600; font-family: 'Titillium Web', sans-serif; color:#fff;">
                        Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet.
                        Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder
                        Junk Ordner.</p>
                    <p>&nbsp;</p>
                    <a href="test">Terms of Use</a>
                </center>
            </div>
        </div>
    </div>

</section>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                <ul class="list-inline mb-2">
                    <li class="list-inline-item">
                        <a href="#">Terms of Use</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item">
                        <a href="#">Privacy Policy</a>
                    </li>
                </ul>
                <p style="color:#c5c5c5!important;" class="text-muted small mb-4">&copy; Campaign-In-One. All Rights
                    Reserved.</p>
            </div>
            <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item mr-3">
                        <a href="#">
                            <i class="fab fa-facebook fa-2x fa-fw"></i>
                        </a>
                    </li>
                    <li class="list-inline-item mr-3">
                        <a href="#">
                            <i class="fab fa-twitter-square fa-2x fa-fw"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                            <i class="fab fa-instagram fa-2x fa-fw"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!-- </Webgains Tracking Code> -->
<?php $config[ TrackingTools ][ EnableWebGains ] ? include_once 'clients/tracking/webgains/webgains_redirect.php' : ' '; ?>

<!-- </Webgains Tracking Code NG> -->
<!-- </Webgains Tracking Code> -->


</body>

</html>
