<?php

/*
 * Webgains Tracking Snippet 
 * 
 * Integrated to redirect.php Page
 * 
*/
?>

<!--Web Tracking-->
<script>
(function (w, e, b, g, a, i, n, s) {
w['ITCLKOBJ'] = a;
w[a] = w[a] || function () {
(w[a].q = w[a].q || []).push(arguments)
}, w[a].l = 1 * new Date();
i = e.createElement(b), n = e.getElementsByTagName(b)[0];
i.async = 1;
i.src = g;
n.parentNode.insertBefore(i, n)
})(window, document, 'script', 'https://analytics.webgains.io/clk.min.js', 'ITCLKQ');
ITCLKQ('set', 'internal.cookie', true);
ITCLKQ('click');
</script>  

<!-- Read the Webgains cookie -->
<script type="text/javascript ">

    // Function to read the cookie
    function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
    }  

    // Check the status of the source 
    var status = "<?= $webgains ?>";
    var lead_ref = "";

    // Check if the status is true and Webgains cookie is set
    if (readCookie('webgains') && status) {
    lead_ref = "<?= $lead_reference ?>";
    console.log("Lead", lead_ref);
    }       
    console.log("webgains", status);
    console.log(readCookie('webgains'));
</script>

<!-- <Webgains Tracking Code> -->
<!-- <Variablendefinitionen> -->
<script language="javascript" type="text/javascript">

    var wgOrderReference = lead_ref;
    var wgOrderValue = "0";
    var wgEventID = 1037645;
    var wgComment = "";
    var wgLang = "de_DE";
    var wgsLang = "javascript-client";
    var wgVersion = "1.2";
    var wgProgramID = 269785;
    var wgSubDomain = "track";
    var wgCheckSum = "";
    var wgItems = "";
    var wgVoucherCode = "";
    var wgCustomerID = "";
    var wgCurrency = "EUR";
    console.log("Lead Reference", lead_ref);
</script>
<!-- </Variablendefinitionen> -->


<!-- <Webgains Tracking Code NG> -->
<script language="javascript" type="text/javascript">
    (function (w, e, b, g, a, i, n, s) {
        w['ITCVROBJ'] = a;
        w[a] = w[a] || function () {
            (w[a].q = w[a].q || []).push(arguments)
        }, w[a].l = 1 * new Date();
        i = e.createElement(b),
                n = e.getElementsByTagName(b)[0];
        i.async = 1;
        i.src = g;
        n.parentNode.insertBefore(i, n)
    })(window, document, 'script', 'https://analytics.webgains.io/cvr.min.js', 'ITCVRQ');
    ITCVRQ('set', 'trk.programId', wgProgramID);
    ITCVRQ('set', 'cvr', {
        value: wgOrderValue,
        currency: wgCurrency,
        language: wgLang,
        eventId: wgEventID,
        orderReference: wgOrderReference,
        comment: wgComment,
        multiple: '',
        checksum: '',
        items: wgItems,
        customerId: wgCustomerID,
        voucherId: wgVoucherCode
    });
    ITCVRQ('conversion');
</script>
