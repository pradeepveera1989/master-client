<?php

/*
 * Google Analytics 
 * 
 * 
*/
?>

<?php

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsKey = $config[TrackingTools][GoogleAnalytics];
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<script type="text/javascript" src="<?php echo $googleURL; ?>"></script>   	
<script async src="<?php echo $googleAnalyticsURL; ?>"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', '<?php echo $googleAnalyticsKey; ?>');
</script>	
