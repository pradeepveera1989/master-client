<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class quiz{
    
    public $config; 
    public $path = "config.ini";
    
    public function __construct() {
        
        $this->config = new config();
        $this->config_quiz = $this->config->getConfigQuiz();
    }
    
    /**
     * Get the Question for the given question index from Config.ini file
     *
     * @param $ques   Question index from config.ini file
     * 
     * @return false - when question index is found in config.ini file
     *         question - when question index is found
     * 
     */     
    public function getQuestion($ques){
        
        if(empty($ques)){
            
            return false;
        }
        
        if(is_null($this->config_quiz[$ques][Question])){
            
            return false;
        }
        
        return $this->config_quiz[$ques][Question];
    }
    

    /**
     * Get the Answer for the given question index from Config.ini file
     *
     * @param $ques   Question index from config.ini file
     * 
     * @return false - when question index is found in config.ini file
     *         Answer - when question index is found
     * 
     */     
    
    public function getAnswer($ques){

        if(empty($ques)){
            return false;
        }
        
        if(is_null($this->config_quiz[$ques][RightAnswer])){
            return false;
        }
        
        return $this->config_quiz[$ques][RightAnswer];        
    }
    
    /**
     * Get the Datatype of the answer for the given question index from Config.ini file
     *
     * @param $ques   Question index from config.ini file
     * 
     * @return false - when question index is found in config.ini file
     *         Datatype - when question index is found
     * 
     */         
    private function getDataType($ques){
        
        if(empty($ques)){
            return false;
        }
        
        if(is_null($this->config_quiz[$ques][DataType])){
            return false;
        }
        
        return $this->config_quiz[$ques][DataType];              
    }        

    /**
     * Get the Options for the given question index from Config.ini file
     *
     * @param $ques   Question index from config.ini file
     * 
     * @return false - when question index is found in config.ini file
     *         Options - when question index is found
     * 
     */         
    
    public function getOptions($ques){

        if(empty($ques)){
            
            return false;
        }
        
        if(is_null($this->config_quiz[$ques][Answers])){
            
            return false;
        }
        
        return $this->config_quiz[$ques][Answers];        
    }    
    
    /**
     * Format all the given list of options with Radio buttons with 'name' as Question Index
     *
     * @param $options   List of Options for the question index in config.ini file
     *        $name      Question index from config.ini file
     * 
     * @return false - when options are empty
     *         array - array of options formated under radio buttons.
     * 
     */         
    private function getOptionList($options, $name){
        
        if(empty($options)){
            return false;
        }
        
        $option = explode(",", $options);

        // suffles the options
        shuffle($option);
        foreach($option as $p){
            
            $html_options .= "<p class='option'><input type='radio' name=$name value='$p' >$p</br></p>";
        }
        return $html_options;
    }

    /**
     * Format an Input field with name as Question index
     *
     * @param $name      Question index from config.ini file
     *        
     * @return false - when no $name is passed
     *         String - input field with name as question index.
     */     
    private function getInputOption($name){
        
        if(empty($name)){
            return false;
        }
        
        return "<input type='text' name=$name >";
    }
    
    /**
     * Formats the given Question with listed options with HTML container
     *
     * @param $question   Question 
     *        $answer     Answer
     *        $options    Alternative options
     *        $name       Question index from config.ini file
     *        
     * @return false - when no question is passed
     *         string - html container with question and options.
     */         
    private function getQuizContainer($question, $answer, $options, $name){
        
        if(empty($question) || empty($answer)){
            
            return false;
        }
        
        $radio ="";
        
        if(!empty($options)){
            $option = $this->getOptionList($options, $name);
        }else{
            $option = $this->getInputOption($name);
        }
        
        return "<div>
                    <div class='form-group'>
                        <div class='col-sm-12'>
                            <div class='input-group question'>
                               <p class='question'>$question</p>
                            </div>
                            <div class='input-group options'>
                               $option
                            </div>
                            <!--div class='input-group answer hidden'>
                               <p class='answer'>$answer</p>
                            </div-->                            
                        </div>       
                    </div>
                </div>";
    }
    
    
    /**
     * Checks if the question exists in config.ini file by considering the 
     * question number from config.ini file
     *
     * @param $number :  Question number (eg: 1,2,3, 4)
     *        
     * @return false - if $number is less or equal to zero
     *               - if No question present with give question number 
     *         true - Question present with given question number.
     * 
     */   
    private function checkQuestionExists($number){
        
        if($number<=0){
            
            return false;
        }
        
        $question = "Quiz".$number; 
        if(is_null($this->config_quiz[$question])){
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Get the quiz container formated with Question , options or input field
     *        
     * @return false - if no 'QuizModeOnOff' is 'Off' in config.ini file
     *         Quiz container -  HTML Formated with question and options
     * 
     */       
    
    public function getQuiz(){
        
        $quiz_container = array();
        if(empty($this->config_quiz)){
            
            return false;
        }
        
        $count = $this->getNumberOfQuestions();

        while($count > 0){
            
            if(!$this->checkQuestionExists($count)){

                return false;
            }     
            $ques = "Quiz".$count;
            #var_dump("Question",$ques);
            $question = $this->getQuestion($ques);
            #var_dump("Question", $question);
            $answer = $this->getAnswer($ques);
            #var_dump("Answer", $answer);
            $options = $this->getOptions($ques);
            #var_dump("Options", $options);
            array_push($quiz_container,  $this->getQuizContainer($question, $answer, $options, $ques));         
            
            $count = $count - 1;
        }
        return $quiz_container;
    }
   
    
    /**
     * Validates the posted answers by User with right answers
     *        
     * @param $answers :  Array of answers supplied by User
     * 
     *         $answers = array(
     *              "Quiz1" => "answer to 1st question",
     *              "Quiz2" => "answer to 2nd question",
     *               .
     *               .
     *          );
     * 
     * @return false - if no answers were supplied
     *         score -  Number of correct answers posted by the user.
     * 
     */         
    public function getScoreForQuiz($answers){
        $score = 0;
        if(empty($answers)){
            return false;
        }
        $ques = array_keys($answers);
        foreach ($ques as $q){
            $right_answer = $this->getAnswer($q);
            $given_answer = $answers[$q];
            $type = $this->getDataType($q);
            if($type){
                settype($given_answer, $type);
                settype($right_answer, $type);
            }

            if($right_answer === $given_answer){
                var_dump($right_answer);
                $score = $score + 1;
            }        
        }
        return $score;
    }
    
    /**
     * Get the number of questions supplied for quiz
     *        
     * @return Number -  Number present under 'QuizQuestion' in config.ini file.
     * 
     */      
    public function getNumberOfQuestions(){
        
        return $this->config_quiz['QuizQuestion'];
    }
}