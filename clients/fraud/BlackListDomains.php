<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of campaign
 *
 * @author Pradeep
 */

class BlackListDomains 
{
    public function __construct()
    {
        $this->table = "`cio.blacklist_domains`";
        $this->dbObject = new db();
    }

    /**
     * getBlackListDomainByName function
     *
     * @param string $domain
     * @return array
     */
    public function getBlackListDomainByName(string $domain) : array
    {
        $return = [];
        if (empty($domain) || !isset($domain) || empty($this->dbObject) || !isset($this->table)) {
            return $return;
        }

        $select = array('*');
        $where = "`domain_name` LIKE '$domain'";

        $result = $this->dbObject->getOne($select, $this->table, $where);   

        if (is_array($result) && !empty($result)) {
            $return = $result;
        }

        return $return;
    }


    /**
     * checkBlackListDomain function
     *
     * @param string $domain_name
     * @return boolean
     */
    public function checkBlackListDomainName(string $domain_name) : bool
    {
        $status = false;
        if (empty($domain_name) || !isset($domain_name)) {
            return $status;
        }

        $result = $this->getBlackListDomainByName($domain_name);

        if (is_array($result) && !empty($result) && isset($result['id']) && (int) $result['id'] > 0) {
            $status = true;
        }

        return $status;
    }
}
