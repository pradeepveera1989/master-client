<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cio_answers
 *
 * @author Pradeep
 */
class cio_answers {
    
    public function __construct() {
        $this->table = "`cio.answers`";
        $this->dbObject = new db();
        $this->config = new config("config.ini");
    }
    
    /*
     * insertSurveyAnswers:
     * 
     * Inserts the Survey answers to the Db
     * 
     * @param type array
     * @return  boolean | int
     */ 
    public function insertSurveyAnswers($params){
        $cio_answer_id = 0;
        $operation = "insert";   
        var_dump("CIO insertSurveyAnswers", $cio_answer_id);
        if(empty($params) || empty(trim($this->table)) || empty(($this->config))){
            return $cio_answer_id;
        }
        
        if(empty(trim($params['answer'])) || ((int)$params['lead_customfield_id']) <= 0){
            return $cio_answer_id;
        }
        
        $created = $this->config->getLogCreatedParams();
        $updated = $this->config->getlogUpdatedParams();
        $answer_option_vals = $this->getAnswerOptionForQuestionId($params);
        
        if(is_bool($answer_option_vals) && $answer_option_vals == FALSE || (int)$answer_option_vals['id'] <= 0){
            return $cio_answer_id;
        }
        $cio_answers = [
            'answer'    =>  $params['answer'],
            'lead_customfield_id'   => $params['lead_customfield_id'],
            'answer_options_id' => $answer_option_vals['id'],
            'created'   => $created['created'],
            'created_by'=> $created['created_by'],
            'updated'   => $updated['updated'],
            'updated_by'   => $updated['updated_by'],
        ];
        $status = $this->dbObject->execute($this->table, $cio_answers, "", $operation);
        if(!empty($status)){
            $cio_answer_id = $this->dbObject->getLastInsertId();
        }

        return $cio_answer_id;
    }
    
    /*
     * getAnswerOptionForQuestionId:
     * 
     * Get the Id of Answer Options for the given Question Id
     * 
     * @param type array
     * @return  array
     *          
     */     
    public function getAnswerOptionForQuestionId($params){
        
        $answer_options = [];
        
        if(empty($params) || empty(trim($params['answer'])) || (int)$params['question_id'] <= 0 || empty($this->dbObject) || empty($params['question_type'])){
            return $answer_options;
        }
        
        $select = array('*');
        $table = "`cio.answer_options`";        
        if($params['question_type'] == 'inputfield'){
            $where = array("`question_id` = ".$params['question_id']);
        }else if($params['question_type'] == 'slider'){
            $where = array("`question_id` = ".$params['question_id']." AND `correct` LIKE 'WAHR'");
        }else{
            $where = array("`question_id` = ".$params['question_id']." AND `value` LIKE '".$params['answer']."'");    
        }
        $result = $this->dbObject->getOne($select, $table, $where);  

        if(empty($result) || (int)$result['id'] <= 0){
            return $answer_options;
        }
        $answer_options = $result;

        return $answer_options;        
    }
    
}
