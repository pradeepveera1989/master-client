<?php


class LeadCustomField
{
    private $table;
    private $dbObject;

    /**
     * __construct function
     */
    public function __construct()
    {
        $this->dbObject = new db();
        $this->table = "`cio.leads_customfield`";
    }
    
    
    /**
     * getCustomField function
     *
     * @param integer $camp_id
     * @param string $type
     * @return array
     */
    public function getCustomField(int $camp_id, string $type):array
    {
        $value = [];

        if ($camp_id <= 0 || empty($type) || empty($this->dbObject) || empty($this->table)) {
            die("getCustomFieldId");
            return $value;
        }

        $select = ['*'];
        $where = "`campaign_id` = $camp_id AND `type` LIKE  '$type'";
        $result= $this->dbObject->getOne($select, $this->table, $where);

        if (is_array($result) && !empty($result)) {
            $value = $result;
        }

        return $value;
    }


    /**
     * getCustomFieldValuesById function
     *
     * @param integer $id
     * @return array
     */
    public function getCustomFieldValuesById(int $id) :array
    {
        $cus_details = [];

        if ($id <= 0) {
            return $cus_details;
        }

        $select = ['*'];
        $where = "`id` = $id";
        $result = $this->dbObject->getOne($select, $this->table, $where);

        if (is_array($result) && !empty($result)) {
            $cus_details = $result;
        }

        return $cus_details;
    }

   
    /**
     * insertCustomLeadField function
     *
     * @param array $ins_value
     * @return boolean
     */
    public function insertCustomLeadField(array $ins_value): int
    {
        $cus_field_id = 0;

        if (empty($ins_value) || !isset($ins_value['campaign_id']) || !isset($ins_value['type'])) {
            die("insertCustomLeadField");
            return $cus_field_id;
        }

        $operation = "insert";
        $where = "";
        $result = $this->dbObject->execute($this->table, $ins_value, $where, $operation, false);

        if (is_bool($result) && $result == true) {
            $return = $result;
            $cus_field_id = $this->dbObject->getLastInsertId();
        }

        return $cus_field_id;
    }


    /**
     * getLeadCustomFieldValues function
     *
     * @param integer $campaign_id
     * @param string $type
     * @param string $datatype
     * @param string $regex
     * @return void
     */
    public function getLeadCustomFieldValues(int $campaign_id, string $type, string $datatype, string $regex = null)
    {
        $cus_val = [];
        if ((int) $campaign_id <= 0 || !isset($type)) {
            return $cus_val;
        }
        $date_time = date("Y-m-d h:i:sa");
        $cus_val = [
            'campaign_id'   => $campaign_id,
            'type'          => $type,
            'datatype'      => $datatype,
            'regex'         => $regex,
            'created_at'    => $date_time,
            'updated_at'    => $date_time
        ];

        return $cus_val;
    }
}
